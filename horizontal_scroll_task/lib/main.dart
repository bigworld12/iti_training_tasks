import 'package:flutter/material.dart';

import 'customIndicator.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: TaskWidget(),
    );
  }
}

class TaskWidget extends StatefulWidget {
  @override
  _TaskWidgetState createState() => _TaskWidgetState();
}

class _TaskWidgetState extends State<TaskWidget> {
  final myController = TextEditingController();
  Widget _createTextField(String init) {
    return SingleChildScrollView(
        padding: EdgeInsets.all(5),
        child: TextField(
            keyboardType: TextInputType.multiline,
            maxLines: 40,
            controller: TextEditingController(text: init)));
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
          child: DefaultTabController(
          length: 4,
          child: Scaffold(
              resizeToAvoidBottomPadding: true,
              appBar: AppBar(
                title: Text("Task RTL"),
              ),
              body: Column(
                children: <Widget>[
                  Expanded(
                      child: TabBarView(children: <Widget>[
                    _createTextField("أهلا"),
                    _createTextField("وسهلا"),
                    _createTextField("مهندس"),
                    _createTextField("أحمد")
                  ])),
                  Align(
                      alignment: FractionalOffset.bottomCenter,
                      child: TabBar(
                          indicator: CustomTabIndicator(Colors.red,MediaQuery.of(context).size.width),
                          indicatorWeight: 5,
                          tabs: <Widget>[
                            Container(),
                            Container(),
                            Container(),
                            Container(),
                          ]))
                ],
              ))),
    );
  }
}
