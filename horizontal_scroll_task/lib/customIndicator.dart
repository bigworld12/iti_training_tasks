import 'package:flutter/material.dart';

class CustomTabIndicator extends Decoration {
  final Color indicatorColor;
  final double parentWidth;
  CustomTabIndicator(this.indicatorColor, this.parentWidth) : super();

  @override
  _CustomPainter createBoxPainter([VoidCallback onChanged]) {
    return new _CustomPainter(this, onChanged,parentWidth);
  }
}

class _CustomPainter extends BoxPainter {
  final CustomTabIndicator decoration;
  final double parentWidth;
  _CustomPainter(this.decoration, VoidCallback onChanged, this.parentWidth)
      : assert(decoration != null),
        super(onChanged);

  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration configuration) {
    assert(configuration != null);
    assert(configuration.size != null);

    //offset is the position from where the decoration should be drawn.
    //configuration.size tells us about the height and width of the tab.
    
    Rect newRect;
    if (configuration.textDirection == TextDirection.ltr)
    newRect = Rect.fromLTRB(0, configuration.size.height, offset.dx + configuration.size.width, 0);    
    else  
    newRect = Rect.fromLTRB(offset.dx, configuration.size.height,parentWidth, 0);   

    final Paint paint = Paint();
    paint.color = decoration.indicatorColor;
    paint.style = PaintingStyle.fill;    
    canvas.drawRect(newRect, paint);
  }
}
