import 'package:flutter/material.dart';

class TaskScreen extends StatefulWidget {
  @override
  _TaskScreenState createState() => _TaskScreenState();
}

class _TaskScreenState extends State<TaskScreen> {
  Widget _createTopCat(String txt) {
    return Padding(padding: EdgeInsets.all(10), child: Text(txt));
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
          appBar: AppBar(
            title: Text("حراج"),
            actions: <Widget>[
              IconButton(icon: Icon(Icons.search), onPressed: () {}),
              IconButton(icon: Icon(Icons.settings), onPressed: () {}),
            ],
          ),
          body: Container(
            child: Column(children: <Widget>[
              SizedBox(
                height: 50,
                child: ListView(
                  shrinkWrap: false,
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    _createTopCat("الرئيسية"),
                    _createTopCat("حراج السيارات"),
                    _createTopCat("حراج العقار"),
                    _createTopCat("حراج الأجهزة"),
                  ],
                ),
              ),
              SizedBox(
                height: 50,
                child: ListView.builder(
                  itemCount: 20,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext bc, int i) {
                    return Padding(
                      padding: EdgeInsets.all(8),
                      child: Text("رقم $i"),
                    );
                  },
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height - 200,
                child: ListView.builder(
                  itemCount: 20,
                  scrollDirection: Axis.vertical,
                  itemBuilder: (BuildContext bc, int i) {
                    return Padding(
                        padding: EdgeInsets.all(3),
                        child: Card(
                            child: Row(children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Image.asset("assets/images/car.png",
                                  fit: BoxFit.contain),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("سيارة مرسيدس S560",style: TextStyle(color: Colors.green.shade900,fontWeight: FontWeight.bold)),
                                Row(children: <Widget>[
                                  Icon(Icons.location_on),
                                  Text("الرياض")
                                ]),
                                Row(children: <Widget>[
                                  Icon(Icons.timer),
                                  Text("قبل 11 دقيقة")
                                ]),
                                Row(children: <Widget>[
                                  Icon(Icons.account_circle),
                                  Text("محمد علي")
                                ])
                              ],
                            )
                          ],
                        )));
                  },
                ),
              )
            ]),
          ),
        floatingActionButton: FloatingActionButton(child: Icon(Icons.add),onPressed: (){

          
        },),
        ),
        );
  }
}
